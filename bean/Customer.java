package com.tcs.ilp.bean;

public class Customer {
	private String customerId;
	private String customerName;
	private String address;
	private String phoneNo = null;
	
	public Customer(String customerId, String customerName, String address,
			String phoneNo) {
		this.customerId = customerId;
		this.customerName = customerName;
		this.address = address;
		this.phoneNo = phoneNo;
	}
	public Customer(String customerId, String customerName, String address) {
		this.customerId = customerId;
		this.customerName = customerName;
		this.address = address;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
}
