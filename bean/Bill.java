package com.tcs.ilp.bean;

public class Bill {
	private String billNo;
	private double billAmount;
	private String Date;
	private String custId;
	public Bill(String billNo, double billAmount, String date, String custId) {
		this.billNo = billNo;
		this.billAmount = billAmount;
		Date = date;
		this.custId = custId;
	}
	public Bill() {
		
	}
	public String getBillNo() {
		return billNo;
	}
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	public double getBillAmount() {
		return billAmount;
	}
	public void setBillAmount(double billAmount) {
		this.billAmount = billAmount;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
}
