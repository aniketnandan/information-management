package com.tcs.ilp.tester;

import java.util.ArrayList;

import com.tcs.ilp.bean.*;
import com.tcs.ilp.dao.BillDao;

public class CustomerBillManagement {
	public static void main(String arg[]){
		BillDao bd = new BillDao();
		ArrayList<Customer> cList = new ArrayList<Customer>();
		
		Customer c1 = new Customer("12f1", "Vivek", "3 no", "9856999888");
		Customer c2 = new Customer("12f2", "Kapoor", "4 no", "9856999101");
		cList.add(c1);
		cList.add(c2);
		
		if(bd.addCustomer(cList)){
			System.out.println("Successfully added!");
		} else {
			System.out.println("not all are added");
		}
		
		ArrayList<Bill> billList = bd.getBillDetails("9856999888");
		for(Bill b : billList) {
			System.out.println("Customer with id " + b.getCustId() +" having bill of amount " + b.getBillAmount() + " and this bill with no "+ b.getBillNo() + " issued on "+b.getDate());
		}
		
		Bill updatedBill = bd.updateBillAmount("243", 7000);
		System.out.println(" updated bill for Customer with id " + updatedBill.getCustId() +" having bill of amount " + updatedBill.getBillAmount() + " and this bill with no "+ updatedBill.getBillNo() + " issued on "+updatedBill.getDate());
		
		bd.getTotalNoOfBillsForCustomer(7000);
		
		double totalBill = bd.getTotalCost("20/02/2016");
		System.out.println("total bill on 20/02/2016 is "+ totalBill);
	}
}	
