package com.tcs.ilp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.tcs.ilp.bean.*;
import com.tcs.ilp.util.util;

public class BillDao {
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet rs = null;

	public boolean addCustomer(ArrayList<Customer> cust) {
		con = util.getConnection();
		if(con != null) {
			String sql = "INSERT INTO CUST_1240932 VALUES (?,?,?,?)";
			int count = 0;
			for(Customer c : cust) {
				try {
					pst = con.prepareStatement(sql);
					pst.setString(1, c.getCustomerId());
					pst.setString(2, c.getCustomerName());
					pst.setString(3, c.getAddress());
					pst.setString(4, c.getPhoneNo());
					count += pst.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				} 
			}
			if(count > 0 && count == cust.size()) {
				closeRelations(pst, con);	
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	public ArrayList<Bill> getBillDetails (String phoneNo) {
		con = util.getConnection();
		ArrayList<Bill> billList = new ArrayList<Bill>();
		Bill b = null;

		if(con != null) {
			String sql = "SELECT b.* FROM BILL_1240932 b INNER JOIN CUST_1240932 c ON b.CustomerID = c.CustomerID AND c.PhoneNo = ? AND c.PhoneNo IS NOT NULL";

			try {
				pst = con.prepareStatement(sql);
				pst.setString(1, phoneNo);
				rs = pst.executeQuery();
				while(rs.next()) {
					b = new Bill();
					b.setBillNo(rs.getString(1));
					b.setBillAmount(rs.getInt(2));
					b.setDate(rs.getDate(3).toString());
					b.setCustId(rs.getString(4));

					billList.add(b);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				closeRelations(pst, con);
			}
			return billList;
		}
		return billList;
	}
	
	public Bill updateBillAmount(String BillNo, double cost) {
		Bill b = null;
		con = util.getConnection();
		if(con != null) {
			String sql = "UPDATE BILL_1240932 SET BillAmount = ? WHERE BillNo = ?";
			try {
				pst = con.prepareStatement(sql);
				pst.setDouble(1, cost);
				pst.setString(2, BillNo);
				if(pst.executeUpdate() > 0) {
					pst = con.prepareStatement("SELECT * FROM BILL_1240932 WHERE BillNo = ?");
					pst.setString(1, BillNo);
					rs = pst.executeQuery();
					if(rs.next()) {
						b = new Bill();
						b.setBillAmount(rs.getDouble(2));
						b.setBillNo(rs.getString(1));
						b.setCustId(rs.getString(4));
						b.setDate(rs.getDate(3).toString());
						return b;
					}				
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				closeRelations(pst, con);
			}
		}
		return null;
	}
	
	public void  getTotalNoOfBillsForCustomer(double billAmount) {
		con = util.getConnection();
		if(con != null) {
			String sql = "SELECT c.CustomerName FROM CUST_1240932 c INNER JOIN BILL_1240932 b ON c.CustomerID = b.CustomerID AND b.BillAmount > ?";
			try {
				pst = con.prepareStatement(sql);
				pst.setDouble(1, billAmount);
				rs = pst.executeQuery();
				
				while(rs.next()){
					System.out.println("customer name is " + rs.getString(1));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				closeRelations(pst, con);
			}
			
		}
	}
	
	public double getTotalCost(String billDate) {
		con = util.getConnection();
		if(con != null){
			String sql =  "SELECT SUM(BillAmount), BillDate FROM BILL_1240932 GROUP BY BillDate HAVING BillDate = TO_DATE(?, 'dd/MM/yyyy')";
			try {
				pst = con.prepareStatement(sql);
				pst.setString(1, billDate);
				rs = pst.executeQuery();
				if(rs.next()){
					return rs.getDouble(1);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				closeRelations(pst, con);
			}
		}
		return 0;
	}

	public void closeRelations(PreparedStatement pst, Connection con) {
		util.closeStatement(pst);
		util.closeConnection(con);
	}
}
